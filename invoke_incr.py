#!/bin/python3


import json
from pathlib import Path
import re
import subprocess
from typing import (
    Iterable,
    BinaryIO,
    Optional,
)
from zipfile import ZipFile
import sys
from tempfile import TemporaryFile
import requests
import time

URL = "https://coveriteam-service.sosy-lab.org/ivaas/run"
SPEC = """
CONTROL AUTOMATON SVCOMP

INITIAL STATE Init;

STATE USEFIRST Init :
  MATCH LABEL [LDV_ERROR] -> ERROR;

END AUTOMATON
"""


def get_diff() -> Iterable[Path]:
    ret = subprocess.run(
        [
            "git",
            "diff",
            "--name-only",
            "--diff-filter=d",
            "HEAD^...",
            "--",
            "*.i",
            "*.c",
        ],
        stdout=subprocess.PIPE,
    )

    output = ret.stdout.decode(errors="ignore")
    print("Git diff: ", output)

    return {Path(path) for path in output.splitlines()}


def record_previous(program: Path, store: BinaryIO):
    subprocess.check_call(["git", "show", f"HEAD^:{program}"], stdout=store)


def call_service(program: Path, previous: Optional[BinaryIO]):

    data = {"program": program.open("rb"), "specification": ("ldv_error.spc", SPEC)}

    if previous is not None:
        data["previous"] = previous
    else:
        data["previous"] = ("predmap.txt", "")

    return requests.post(URL, files=data)


def determine_result(run):
    """
    It assumes that any verifier or validator implemented in CoVeriTeam
    will print out the produced aftifacts.
    If more than one dict is printed, the first matching one.
    """
    verdict = None
    verdict_regex = re.compile(r"'verdict': '([a-zA-Z\(\)\ \-]*)'")

    for line in reversed(run):
        line = line.strip()
        verdict_match = verdict_regex.search(line)
        if verdict_match and verdict is None:
            # CoVeriTeam outputs benchexec result categories as verdicts.
            verdict = verdict_match.group(1)
        if "Traceback (most recent call last)" in line:
            verdict = "EXCEPTION"
    if verdict is None:
        return "UNKNOWN"
    return verdict


def handle_response(tool: str, response: requests.Response):
    output_path = Path(f"output-{tool}.zip")
    with output_path.open("w+b") as fd:
        fd.write(response.content)
        fd.flush()
        fd.seek(0)
        with ZipFile(fd, "r") as zipf, zipf.open("LOG") as log:
            cvt_log = log.read().decode(errors="ignore")
            print(
                "-------------------------------------------------------------------------\n"  # noqa E501
                "The following log was produced by CoVeriTeam "
                "on the server: %s\n"
                "-------------------------------------------------------------------------\n"  # noqa E501
                "END OF THE LOG FROM REMOTE EXECUTION",
                cvt_log,
            )
            return determine_result(cvt_log.splitlines())


def check_file(program: Path):

    name = program.stem
    with TemporaryFile("w+b") as tmpf:
        has_previous = True
        try:
            record_previous(program, tmpf)
        except subprocess.CalledProcessError:
            print("No previous version found")
            has_previous = False

        tmpf.seek(0)

        ret = None
        ts = time.time()
        if has_previous:
            ret = call_service(program, tmpf)
        else:
            ret = call_service(program, None)
        td = time.time() - ts
        print(f"Call to service took: {td:.1f}s")  # noqa T201

        if ret.status_code != 200:
            try:
                message = ret.json()["message"]
                print(message)  # noqa T201
            except (KeyError, json.JSONDecodeError):
                lines = ret.content.decode(errors="ignore")
                print("There was an error:\n%s", lines)  # noqa T201
            sys.exit(1)

        verdict = handle_response(name, ret)

        if verdict != "true":
            print("Verdict was not true. Aborting...")  # noqa T201
            sys.exit(1)


def main():

    for program in get_diff():
        check_file(program)


if __name__ == "__main__":
    main()
